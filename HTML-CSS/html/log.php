﻿<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Documento sem título</title>
    </head>

    <body>
        <h1>Parâmetros</h1>

        <?php
        $parametros = file_get_contents('php://input') . '&' . $_SERVER['QUERY_STRING'];
        $parametros = explode('&', $parametros);

        echo '<ul>';

        foreach ($parametros as $param) {
            if (!empty($param)) {
                echo '<li>';
                echo urldecode($param);
                echo '</li>';
            }
        }

        echo '</ul>';
        ?>

    </body>
</html>