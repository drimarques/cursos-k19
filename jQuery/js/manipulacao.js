$(document).ready(function() {

   $("#div1").css({
      "border": "2px solid black",
      "width": "800px",
      "heigth": "200px",
      "margin": "15px",
      "padding": "15px"
   });

   //adiciona uma classe ao um elemento
   $("#addClass").click(function() {
      $("#div1").addClass("destaque");
   });

   //Remove a classe de um elemento
   $("#removeClass").click(function() {
      $("#div1").removeClass("destaque");
   });

   //Adiciona ou remove a classe
   $("#toggleClass").click(function() {
      $("#div1").toggleClass("destaque");
   });

   //
   $("#hasClass").click(function() {
      var destaque = $("#div1").hasClass("destaque");
      alert(destaque ? "Com destaque" : "Sem destaque");
   });

});