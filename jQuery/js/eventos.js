//Página 383

$(document).ready(function(){

   console.log("ready");
   
   
   /*Tratando o evento resize, quando o elemento é redimencionado*/
   $(window).resize(function(){
      var largura = $(window).width();
      var altura = $(window).height();
      console.log("resize: (" + largura + ", " + altura + ")");
   });
   
   //Quando a página é rolada, scroll
   $(window).scroll(function(){
      var x = $(window).scrollLeft();
      var y = $(window).scrollTop();
      console.log("scroll: (" + x + ", " + y + ")");
   });
   
   //Tratando foco nos elementos
   $("*").focus(function(){
      var tagName = this.tagName;
      var id = this.id;
      console.log("focus: (" + tagName + ", #" + id + ")");
   });
   
   //Tratando focusin, mostra todos elementos que estão dentro
   $("*").focusin(function(){
      var tagName = this.tagName;
      var id = this.id;
      console.log("focus: (" + tagName + ", #" + id + ")");
   });
   
   //Tratando evento blur... quando o elemento perde o foco...
   $("*").blur(function(){
      var tagName = this.tagName;
      var id = this.id;
      console.log("blur: (" + tagName + ", #" + id + ")");
   });
   
   //Quando o elemento ou um dos seus descendentes perde o foco... ZUADO
   $("*").focusout(function(){
      var tagName = this.tagName;
      var id = this.id;
      console.log("focusout: (" + tagName + ", #" + id + ")");
   });
   
   
   //Parei no evento select
   
});