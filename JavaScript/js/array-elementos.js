//percorrendo array
//var cursos = ["K01","K02","K03","K04","K05"];
//
//for (var i = 0; i < cursos.length; i++){
//    console.log(cursos[i]);
//};


//array add
//var cursos = ["K01","K02","K03","K04","K05"];
//cursos[cursos.length] = "K01"; //adicionando na ultima posicao
//
//for (var i = 0; i < cursos.length; i++){
//    console.log(cursos[i]);
//}


//array add com push
//var cursos = ["K01","K02","K03","K04","K05"];
//cursos.push("K01");
//
//for (var i = 0; i < cursos.length; i++){
//    console.log(cursos[i]);
//}


//Array concat = juntando array
//var formacaoJava = ["K01","K02"];
//var formacaoJavaAvancado = ["K03","K04","K05"];
//
//var formacaoCompleta = formacaoJava.concat(formacaoJavaAvancado);
//
//for (var i = 0; i < formacaoCompleta.length; i++){
//    console.log(formacaoCompleta[i]);
//}


//array recebendo a ultima posição
//var cursos = ["K01","K02","K03","K04","K05"];
//var curso = cursos.pop();
//console.log(curso);


var cursos = ["K01","K02","K03","K04","K05"];
cursos.reverse();

for (var i = 0; i < cursos.length; i++){
    console.log(cursos[i]);
}