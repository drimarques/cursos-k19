var titulo = document.getElementById("titulo"); //titulo recebendo o get do elemento que tem id titulo
console.log(titulo.tagName + ": " + titulo.innerHTML);

var array = document.getElementsByName("categoria");
for(var i = 0; i < array.length; i++){
    console.log(array[i].tagName + ": " + array[i].id);
}

var array = document.getElementsByTagName("label");
for(var i = 0; i < array.length; i++){
    console.log(array[i].tagName + ": " + array[i].getAttribute("for"));
}

var array = document.getElementsByClassName("centralizado");
for(var i = 0; i < array.length; i++){
    console.log(array[i].tagName + ": " + array[i].id);
}

//var formulario = document.querySelectorAll("body > form");
//console.log(formulario.tagName + ": " + formulario.getAttribute("action"));


var array = document.querySelectorAll("label");
for(var i = 0; i < array.length; i++){
    console.log(array[i].tagName + ": " + array[i].getAttribute("for"));
}
