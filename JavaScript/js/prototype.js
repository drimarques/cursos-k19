var curso = {sigla: "K11", nome: "Orientação a objetos em JavaScript"};

var novoCurso = Object.create(curso);

//exibe K11
console.log(novoCurso.sigla);

//exibe Orientação a objetos em JavaScript
console.log(novoCurso.nome);


//adicionando uma nova propriedade
novoCurso.cargaHoraria = "60 horas";

console.log(novoCurso.cargaHoraria);

console.log("Curso: " + curso.cargaHoraria);

